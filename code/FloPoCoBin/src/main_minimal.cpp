/*
  A minimal example on how use libFloPoCo to generate operators for you.

  In the FloPoCo distribution it is built into executable fpadder_example

  To compile it independently, do something like this 
  
  g++ -std=c++17 -I[FLOPCO_SOURCES]/code/HighLevelCores/include -I[FLOPOCO_SOURCES]/code/VHDLOperators/include -o main_minimal.cpp.o -c main_minimal.cpp 
  g++ main_minimal.o [FLOPOCO_BUILD]/code/HighLevelCores/libhileco.so [FLOPOCO_BUILD]/code/VHDLOperators/libFloPoCO.so -o fpadder_example

  This file is part of the FloPoCo project
  developed by the Arenaire team at Ecole Normale Superieure de Lyon
  
  Author : Florent de Dinechin, Florent.de-Dinechin@insa-lyon.fr

  Initial software.
  Copyright © ENS-Lyon, INRIA, CNRS, UCBL,  
  2011.
  All rights reserved.

*/

#include "flopoco/UserInterface.hpp"
#include "flopoco/Targets/Virtex6.hpp"
#include "flopoco/FPAddSub/FPAddSinglePath.hpp"

using namespace flopoco;



int main(int argc, char* argv[] )
{

	Virtex6 target{};

	target.setFrequency(200e6); // 200MHz
	int wE = 9;
	int wF = 33;

	try{
		Operator*	op = new FPAddSinglePath(nullptr, &target, wE, wF);
		// if we want pipeline we need to add the two following lines
		op->schedule(); 
		op->applySchedule();
		
		ofstream file;
		
		file.open("FPAdd.vhdl", ios::out);
		
		op->outputVHDLToFile(file);
		
		file.close();
	
	
		cerr << endl<<"Final report:"<<endl;
		op->outputFinalReport(cout, 0);
	}
	catch(string &e){
		cerr << "FloPoCo raised the following exception: " << e << endl;
	}
	return 0;
}



