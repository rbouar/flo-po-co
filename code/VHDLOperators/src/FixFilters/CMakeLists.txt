add_flopocolib_src(
	FixFIR.cpp
        IntFIRTransposed.cpp
    FixHalfSine.cpp
	FixIIR.cpp
	FixIIRShiftAdd.cpp
	FixRootRaisedCosine.cpp
	FixSOPC.cpp
)
